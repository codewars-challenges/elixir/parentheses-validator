defmodule ParenthesesValidator do
  def valid_parentheses(string) do
    string
    |> String.split("", trim: true)
    |> parse()
  end

  defp parse(parentheses), do: parse(parentheses, 0)
  defp parse(["(" | tail], open_parentheses), do: parse(tail, open_parentheses + 1)
  defp parse([")" | tail], open_parentheses) when open_parentheses > 0, do: parse(tail, open_parentheses - 1)
  defp parse([")" | _tail], open_parentheses) when open_parentheses <= 0, do: false
  defp parse([_ | tail], open_parentheses), do: parse(tail, open_parentheses)
  defp parse([], open_parentheses), do: open_parentheses == 0
end

validate = &ParenthesesValidator.valid_parentheses/1
validate.("()")
|> IO.inspect()
validate.(")(()))")
|> IO.inspect()
validate.("(")
|> IO.inspect()
validate.("(())((()())())")
|> IO.inspect()
